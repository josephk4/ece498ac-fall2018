"""
# Zero Knowledge Proofs in Python

Examples of discrete-log zero-knowledge proofs implemented in Python

More specifically, these are non-interactive, zero-knowledge,
proofs of knowledge. They can be analyzed and proven secure
in the random oracle model (the random oracle here is instantiated
with the SHA2 hash function).

Lecture notes:
   http://soc1024.ece.illinois.edu/teaching/ece498am/fall2017/
   https://www.cs.jhu.edu/~susan/600.641/scribes/lecture10.pdf
   https://www.cs.jhu.edu/~susan/600.641/scribes/lecture11.pdf
   http://soc1024.web.engr.illinois.edu/teaching/ece598am/fall2016/zkproofs.pdf
   
You must fill in the portions labelled #TODO. See the README.md in this
directory for submission instructions. Points are awarded as marked.
Total possible points (not including bonus): 105
"""


"""
## Import Elliptic Curves

The zero-knowledge proof schemes we work through here 
 can work with any DLog group. This implementation makes use of
the secp256k1 elliptic curve group. We call an element of this group
(i.e., a point on the curve), simply a Point.

The order of this group, p, is a 256-bit prime number. Furthermore, p
happens to be extremely close to 2^256. Because of this, we can sample
exponents easily by choosing a random 32-byte number, and with high probability,
will be within [0,p).
   uint256_from_str(rnd_bytes(32)) is an exponent.

Sometimes this will be represented by the object Fp, which automatically handles
arithmetic modulo p. The underlying 'long' value can be extracted as `p.n` if 
`type(p) is Fp`.
"""

import secp256k1
from secp256k1 import Point, q, Fq, order, p, Fp, G, curve, ser, deser, uint256_from_str, uint256_to_str, make_random_point
import os, random

# p is the order (the # of elements in) the group, i.e., the number of points on the curve
# order = p = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141
print order
print Fp  # Fp is the group of exponents (integers mod p)

# ser/deser: convert Point -> string and vice versa
#   ser : Point -> str, deser : str -> Point

"""
"""
print repr(G)
print repr(p * G)
print deser(ser(G))

Hx = Fq(0x8E3871A594F9AF7A1F357A0793124AAF3358B0F020983678BCD411EE6AF387A5L)
Hy = Fq(0x83BFAA8176272D0E4D7AD3577F3A0A3B70D7E1BFC2B638CA2807562F2CA85F59L)
H = Point(curve, Hx,Hy)
# H = random_point(seed=sha2("H")) # An alternate generator

"""
## Random Oracle Functions
We need ways of sampling random strings, random integers in Z_p,
and random points in the group.
"""

## Default random oracle
def make_random_oracle():
    """This function returns a new random oracle, `RO`.
    The random oracle maps arbitrary-length input strings `s` to
    a 32-byte digest. It is initialized with an empty dictionary. 
    Each time RO is queried with a new value, a
    random response is sampled and stored."""
    
    _mapping = {}
    def RO(s):
        assert type(s) is str
        if not s in _mapping:
            _mapping[s] = os.urandom(32)
        return _mapping[s]
    return RO

# Random oracle instantiated with SHA2 hash
def sha2(x):
    from Crypto.Hash import SHA256
    return SHA256.new(x).digest()


"""
## Preliminary example: Proof of knowledge of discrete logarithm

In this part, we provide a scheme offers a discrete log proof of `ZKP{ (a): A = a*G }`.

Note that the statement `A` is a parameter to the scheme, as it must
be known to both the prover and verifier.

The Prover takes several additional arguments:

 - `rnd_bytes`, such that `rnd_bytes(n)` returns an `n`-byte random string. By default, will use the operating system os.urandom. 

    (Note: as this function is non-blocking, may be a poor choice if the OS runs out of entropy)

 - RO, a random oracle, such that `RO(s)` where `s` is an arbitrary length string, returns a randomly chosen value. By default, will use the sha2 hash.

These can be overridden in later section as part of the security proof constructions.
"""
def dlog_prover(A, a, rnd_bytes=os.urandom, RO=sha2):
    assert a*G == A

    # blinding factor
    k = uint256_from_str(rnd_bytes(32)) % order

    # commitment
    K = k*G

    # Invoke the random oracle to receive a challenge
    c = uint256_from_str(RO(ser(K)))

    # response
    s = Fp(k + c*a)

    return (K,s)


def dlog_verifier(A, prf, RO=sha2):
    (K,s) = prf
    assert type(A) is type(K) is Point
    assert type(s) is Fp

    # Recompute c w/ the information given
    c = uint256_from_str(RO(ser(K)))

    # Check the verification condition
    assert s.n *G == K + c*A
    return True


def dlog_test():
    a = uint256_from_str(os.urandom(32))
    A = a*G
    prf = dlog_prover(A, a)
    assert dlog_verifier(A, prf)
    print 'Dlog correctness test complete!'

dlog_test()

"""
## Part 1: Make a Pedersen commitment to your crypto egg. 
 Provide a ZK proof that your commitment is correct.

   Zk{ (x,r): X = x*G, C = x*G + r*H }

By completing this proof, you prove you still have knowledge of your egg!
without revealing which.

The verifier is provided for you. (Since we will publicly verify the proofs). You must complete the prover.
"""

def make_pedersen_commitment(x, rnd_bytes=os.urandom):
    r = uint256_from_str(rnd_bytes(32))
    C = x * G + r * H
    return C, r

def pedersen_prover(C, X, x, r, rnd_bytes=os.urandom, RO=sha2):
    """
    Params: 
       x and r are elements of Fp
       C,X are Points
    Returns:
       prf, of the form (KX,KC,sx,sr)
    """
    assert X == x * G
    assert C == x * G + r * H

    # TODO: fill in your code here (10 points)

    return (KX,KC,sx,sr)

def pedersen_verifier(C, X, prf, RO=sha2):
    (KX,KC,sx,sr) = prf
    assert type(KX) == type(KC) == Point
    assert type(sx) == type(sr) == Fp

    # Recompute c w/ the information given
    c = uint256_from_str(RO(ser(KX) + ser(KC)))

    assert sx.n *G            == KX + c*X
    assert sx.n *G + sr.n *H  == KC + c*C
    return True

def pedersen_test():
    
    x = uint256_from_str(os.urandom(32))
    X = x * G
    C,r = make_pedersen_commitment(x)

    prf = pedersen_prover(C, X, x, r)
    (KX, KC, sx, sr) = prf
    print repr((ser(C), ser(KX),ser(KC),uint256_to_str(sx.n).encode('hex'),uint256_to_str(sr.n).encode('hex')))
    
    assert pedersen_verifier(C, X, prf)
    print "Pedersen correctness test complete!"

pedersen_test()


"""
## Part 2. Arithmetic relations

Example: a more complicated discrete log proof
      Zk{ (a, b):  A=a*G, B=b*G,  C = (a*(b+3)) * G }

First rewrite as:
      Zk{ (a, b):  A=a*G, B=b*G,  (C-3*A) = b*A) }

You need to implement a prover and verifier for the above scheme.
"""

def arith_prover(a, b, A, B, C, rnd_bytes=os.urandom, RO=sha2):
    """
    Params: 
       a and b are elements of Fp
       A, B, C are Points
    Returns:
       prf, of the form (KA,KB,KC,sa,sb)

    Must satisfy verify_proof2(A, B, C, prf)
    Must be zero-knowledge
    """
    assert a*G == A
    assert b*G == B
    assert (a*(b+3))*G == C

    # TODO: fill in your code here (10 points)

def arith_verifier(A, B, C, prf, rnd_bytes=os.urandom, RO=sha2):
    (KA,KB,KC,sa,sb) = prf
    assert type(KA) == type(KB) == type(KC) == Point
    assert type(sa) == type(sb) == Fp

    # TODO: fill in your code here (10 points)

def arith_test():
    # Randomly choose "a" and "b"
    a = uint256_from_str(os.urandom(32))
    b = uint256_from_str(os.urandom(32))
    A = a*G
    B = b*G
    C = (a*(b+3)) * G

    prf = arith_prover(a, b, A, B, C)
    assert arith_verifier(A, B, C, prf)
    print "Arithmetic Relation correctness test complete"

arith_test()

"""
## Part 3. OR composition

In this part you will need to prove knowledge of one of two possible secrets,

   Zk{ (a,b): A = a*G    OR    B = b*G }

without revealing which one it is you know.

The verifier is provided for you.
"""

def OR_prover(A, B, x, rnd_bytes=os.urandom, RO=sha2):
    assert x*G == A or x*G == B

    # TODO: Fill your code in here (20 points)

    return (KA,KB,sa,sb,ca,cb)

def OR_verifier(A, B, prf, RO=sha2):
    (KA,KB,sa,sb,ca,cb) = prf
    assert type(KA) is type(KB) is Point
    assert type(sa) is type(sb) is Fp

    # Check the challenges are correctly constrained
    c = uint256_from_str(RO(ser(KA) + ser(KB)))
    assert (ca + cb) % p == c

    # Check each proof the same way
    assert sa.n *G == KA + ca*A
    assert sb.n *G == KB + cb*B

    return True

def OR_test1():
    # Try first way
    a = uint256_from_str(os.urandom(32))
    A = a*G
    B = make_random_point()

    prf = OR_prover(A, B, a)
    assert OR_verifier(A, B, prf)
    print "OR composition correctness 1 test complete!"

def OR_test2():
    # Try second way
    b = uint256_from_str(os.urandom(32))
    A = make_random_point()
    B = b*G

    prf = OR_prover(A, B, b)
    assert OR_verifier(A, B, prf)
    print "OR composition correctness 2 test complete!"

OR_test1()
OR_test2()


"""
## Part 4. Schnorr signature

  We can write a Schnor signature as:

     SoK[m] { (x): X = x*G }

  Similar to part 1, except we the challenge is derived from the message.
"""
def schnorr_sign(x, m, rnd_bytes=os.urandom, RO=sha2):
    assert type(x) is str
    assert type(m) is str

    # TODO: Your code goes here (10 points)

def schnorr_verify(X, m, sig, RO=sha2):
    assert type(X) is Point
    assert type(sig) is str and len(sig) is 65
    (K,s) = deser(sig[:33].encode('hex')), uint256_from_str(sig[33:])
    c = uint256_from_str(RO(ser(K) + sha2(m)))
    assert s *G == K + c*X
    return True

def schnorr_test():
    msg = "hello"

    x = os.urandom(32)
    X = uint256_from_str(x) * G
    
    sig = schnorr_sign(x, msg)
    assert schnorr_verify(X, msg, sig)
    print "Schnorr Test complete"

schnorr_test()


"""
## Part 5. Range proofs

- Create a proof that C is a commitment to a, and a is in the range [0,31].

    Zk{ (a, r): C = g^a h^r and  0 <= a <= 31 }

  Hint: You can implement this by creating commitments to the binary expansion
    of a, and then proving the following:

    Zk{ (b0, b1, ... b4, r, r0, r1, ..., r4):
               A = g^(b0 + 2*b1 + ... + 16*b4) h^r
               and  (C0 = g^(b0) h^r0) ...
               and  (C0 = g h^r0 OR C0 = h^r0) ... }

  Hint: to avoid too much repetition, consider implementing the Bonus question first.

"""
def range_prover(a, r, C, rnd_bytes=os.urandom, RO=sha2):
    assert type(C) is Point
    assert a*G + r*H == C
    
    # TODO: fill in your code here (10 points)

def range_verifier(C, prf, rnd_bytes=os.urandom, RO=sha2):
    assert type(C) is Point

    # TODO: fill in your code here (10 points)


"""
## Part 6: Extractor and simulator

In this part, you will implement in code a portion of the security proof
for the discrete log proof scheme from the Preliminary.
"""
def dlog_extractor(A, Adv):
    assert type(A) is Point

    ## Step 1: run the adversary to generate a proof while recording
    ## the random bits and oracle queries

    # TODO: Fill your code in here (5 points)

    ## Step 2: run the adversary again, replaying the random bits,
    ## and intercepting the call to the random oracle

    # TODO: Fill your code in here (5 points)

    ## Step 3: Extract a witness from the two proofs and oracle queries
    
    # TODO: Fill your code in here (5 points)

def dlog_test_extractor():
    # Make a test case based on running the real prover
    a = uint256_from_str(os.urandom(32))
    A = a * G
    
    def Adv(A, rnd_bytes, RO):
        assert A == a * G
        return dlog_prover(A, a, rnd_bytes, RO)

    a_ = dlog_extractor(A, Adv)
    assert a == a_
    print 'Extractor test complete!'

def dlog_test_extractor_harder():
    # Make a test case based on running a "picky" prover
    a = uint256_from_str(os.urandom(32))
    A = a * G
    
    def Adv(A, rnd_bytes, RO):
        assert A == a * G

        while True:
            # The "picky" prover loops until it is happy

            # Make a whimsical decision
            coin = rnd_bytes(1)
            if ord(coin) < 128: continue

            k = uint256_from_str(rnd_bytes(32)) % order
            K = k*G
            c = uint256_from_str(RO(ser(K)))

            # I only like challenges that end with 3 zero bits
            if c & 0b111 != 0: continue

            # OK I'm satisfied
            s = Fp(k + c*a)
            return (K, s)

    a_ = dlog_extractor(A, Adv)
    assert a == a_
    print 'Extractor test complete!'

dlog_test_extractor()
dlog_test_extractor_harder()

def dlog_simulator(A, rnd_bytes):
    """
    Returns:
    - (prf, RO)
    - prf, a tuple of the form (K,s),
        where K is a Point
        and s is an element of Fp
    - RO is a random oracle function
    
    The following must hold:
    - RO is a mapping from to 32-bit strings. Its output should be indistinguishable from an ordinary random oracle generated from `make_random_oracle`.
    """
    # TODO: Fill in your code here (10 points)

def dlog_test_simulator():
    rnd_bytes = os.urandom
    # Test with a randomly generated point on curve
    A = make_random_point(rnd_bytes)

    (prf, RO) = dlog_simulator(A, rnd_bytes)

    # The proof must verify
    assert dlog_verifier(A, prf, RO=RO)
    print "DLOG simulator test complete!"

dlog_test_simulator()


"""
## Bonus Challenge: Expression compiler

  Can you take a description of a ZK scheme in Camenisch-Stadler notation,
  and automatically generate the protocol for it?

  Read the ZKPDL paper for inspiration (that is a much more full-featured system!)

  To complete this bonus, the goal should be to support the following
  grammar for expressing ZK constraint systems:

    ZK := EQN | ZK and ZK | ZK or ZK
    EQN := BEXP = BEXP
    BEXP := BEXP * BEXP | BEXP / BEXP | BEXP ^ EEXP | BIDENT
    EEXP := EEXP + EEXP | EEXP - EEXP | EEXP * EEXP | EEXP / EEXP | EIDENT | ELIT
    BIDENT := 'G', 'H', ...
    EIDENT := 'a','b', ...
    ELIT := 1,2,3, ...

  Example:
   "Zk{ (a,b): A = G^(a) * (H^10)^(3*b)"

  would be expressed as:
  ZK(
   EQN( 
    BEXP( BIDENT('A') )
   = 
    BEXP( 
     BEXP( BIDENT('G') ^ EEXP( EIDENT('a') ) )
    *
     BEXP( 
      BEXP( BEXP(BIDENT('H')) ^ EEXP(ELIT(10)) )
     ^
      EEXP(ELIT(3) * EIDENT('b'))
     )
    )
   )
  )

  Hints:
  - Build an abstract syntax tree to traverse.
  - Flatten AND and OR to a single list
  - Apply operations to left and right sides until left side has no exponents
  - To handle multiplications, introduce ephemeral commitments (like in Part 5)

    For example, to prove
      Zk{ (a,b,c): A = g^a, B = g^b, C = g^c, D = g^(abc) }

    you could introduce an ephemeral commitment:

      E = g^(ab) h^r, for random r

    Then the extended proof is:
      Zk{ (a,b,c,r,r2): A = g^a, B = g^b, C = g^c, E = A^b h^r, D = E^c h^r2 }

    where the prover sets r2 = -(c*r).

    This now has the property that the only EEXP terms are EEXP(EIDENT(_)).
"""
